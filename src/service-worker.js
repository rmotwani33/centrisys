importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.2.0/workbox-sw.js');

var ITEM_ENDPOINT = "https://cms.centrisys-cnp.com/files?fields=filename_disk&fields=title&&fields=type";
var rootDomain = "https://cms.centrisys-cnp.com/assets/";

var mediaItems = new Array(); //all data, filename, extension and title | not used 
var mediaFiles = new Array(); //just the filename
var rootPages = new Array(); //seperate page json files

var date = new Date();
var longTime = date * 1;

var CACHE_PREFIX = "CENT_";

var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
};

var appVersionNumber = 0;

var galleriesCache = CACHE_PREFIX + "Gallery";
var sectionsCache = CACHE_PREFIX + "Sections";
var pagesCache = CACHE_PREFIX + "Pages";
var staticFilesCache = CACHE_PREFIX + "StaticFiles";
var mediaCaches = CACHE_PREFIX + "Media";

addEventListener('fetch', event => {

    event.waitUntil(async function () {
        if (!event.clientId) return;
        const client = await clients.get(event.clientId);
        if (!client) return;

        client.postMessage({
            msg: "Fetch",
            url: event.request.url
        });

    }());
});

workbox.setConfig({ debug: true })
workbox.googleAnalytics.initialize();
workbox.core.clientsClaim();

var myHeaders = new Headers();
myHeaders.append("Authorization", "Bearer This_is_my_very_secure_token_for_access");


//static files
var filesToCache = [
    'service-worker.js',
    'index.html',
    'js/jquery-3.4.0.min.js',
    'js/app.js',
    'css/main.min.css',
];

self.addEventListener('install', event => {
    console.log("installing");
    console.log("firing with " + appVersionNumber);

    event.waitUntil(
        caches.open(mediaCaches).then(function (cache) { //cache all the media files
            fetch("https://cms.centrisys-cnp.com/files?fields=filename_disk&fields=title&&fields=type", requestOptions)
                .then(response => response.json())
                .then(data => {
                    console.log("CACHING MEDIA " + appVersionNumber);
                    for (const media of data.data) {
                        mediaFiles.push(media.filename_disk);
                    }
                    for (var i = 0; i < mediaFiles.length; i++) {
                        var mediaFile = "https://cms.centrisys-cnp.com/assets/" + mediaFiles[i];
                        cache.add(mediaFile);
                    }
                }).catch(error => console.log('error', error));
        }),
        caches.open(staticFilesCache).then(function (cache) { //cache all the static files
            console.log("caching static files");
            //cache.addAll(filesToCache);
            for (const file of filesToCache) {
                cache.add(file);
            }
        }),
        caches.open(pagesCache).then(function (cache) { //cache all the json pages
            console.log("CACHING PAGES " + appVersionNumber);
            var myPages = [];
            fetch("https://cms.centrisys-cnp.com/items/page", requestOptions)
                .then(function (response) {
                    return response.json();
                }).then(function (data) {
                    for (const page of data.data) {
                        var pageRoot = "https://cms.centrisys-cnp.com/items/page/" + page.id + "?fields=*.*";
                        myPages.push(pageRoot);
                        console.log("pushing page root " + pageRoot);
                        cache.add(pageRoot  );
                    }
                });
        }),
        caches.open(sectionsCache).then(function (cache) { //cache all the json page sections
            console.log("CACHING SECTIONS " + appVersionNumber);
            var contentSections = [];
            fetch("https://cms.centrisys-cnp.com/items/content_section", requestOptions)
                .then(function (response) {
                    return response.json();
                }).then(function (data) {
                    for (const section of data.data) {
                        var contentSection = "https://cms.centrisys-cnp.com/items/content_section/" + section.id + "?fields=*.*";
                        console.log("Content Section! " + contentSection + "!!!!!!!!!!!!!");
                        contentSections.push(contentSection);
                        cache.add(contentSection);
                    }
                });
        }),
        caches.open(galleriesCache).then(function (cache) { //cache all the json page sections
            console.log("CACHING GALLERIES " + appVersionNumber);
            var gallerySections = [];
            fetch("https://cms.centrisys-cnp.com/items/media_item", requestOptions)
                .then(function (response) {
                    return response.json();
                }).then(function (data) {
                    for (const gallery of data.data) {
                        var gallerySection = "https://cms.centrisys-cnp.com/items/media_item/" + gallery.id + "?fields=*.*";
                        //console.log("Content Section! " + contentSection);
                        gallerySections.push(gallerySection);
                        cache.add(gallerySection);
                    }
                });
        }),
    )
});

workbox.routing.registerRoute(
    ({ url }) => url.pathname.startsWith('/app/v2/'),
    new workbox.strategies.NetworkFirst({
        cacheName: CACHE_PREFIX + 'pages',
        plugins: [
            new workbox.rangeRequests.RangeRequestsPlugin(),
            new workbox.expiration.ExpirationPlugin({
                maxEntries: 500,
                maxAgeSeconds: 60, // 1 minute
            }),
        ],
        matchOptions: {
            ignoreVary: true
        }
    })
);