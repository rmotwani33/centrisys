(function (cjs, an) {
    console.log("call")
    var p; // shortcut to reference prototypes
    var lib={};var ss={};var img={};
    lib.webFontTxtInst = {}; 
    var loadedTypekitCount = 0;
    var loadedGoogleCount = 0;
    var gFontsUpdateCacheList = [];
    var tFontsUpdateCacheList = [];
    lib.ssMetadata = [
            {name:"centrisys_map_atlas_1", frames: [[0,0,1558,750]]}
    ];
    
    
    
    lib.updateListCache = function (cacheList) {		
        for(var i = 0; i < cacheList.length; i++) {		
            if(cacheList[i].cacheCanvas)		
                cacheList[i].updateCache();		
        }		
    };		
    
    lib.addElementsToCache = function (textInst, cacheList) {		
        var cur = textInst;		
        while(cur != null && cur != exportRoot) {		
            if(cacheList.indexOf(cur) != -1)		
                break;		
            cur = cur.parent;		
        }		
        if(cur != exportRoot) {		
            var cur2 = textInst;		
            var index = cacheList.indexOf(cur);		
            while(cur2 != null && cur2 != cur) {		
                cacheList.splice(index, 0, cur2);		
                cur2 = cur2.parent;		
                index++;		
            }		
        }		
        else {		
            cur = textInst;		
            while(cur != null && cur != exportRoot) {		
                cacheList.push(cur);		
                cur = cur.parent;		
            }		
        }		
    };		
    
    lib.gfontAvailable = function(family, totalGoogleCount) {		
        lib.properties.webfonts[family] = true;		
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
        for(var f = 0; f < txtInst.length; ++f)		
            lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		
    
        loadedGoogleCount++;		
        if(loadedGoogleCount == totalGoogleCount) {		
            lib.updateListCache(gFontsUpdateCacheList);		
        }		
    };		
    
    lib.tfontAvailable = function(family, totalTypekitCount) {		
        lib.properties.webfonts[family] = true;		
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
        for(var f = 0; f < txtInst.length; ++f)		
            lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		
    
        loadedTypekitCount++;		
        if(loadedTypekitCount == totalTypekitCount) {		
            lib.updateListCache(tFontsUpdateCacheList);		
        }		
    };
    (lib.AnMovieClip = function(){
        this.actionFrames = [];
        this.ignorePause = false;
        this.gotoAndPlay = function(positionOrLabel){
            cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
        }
        this.play = function(){
            cjs.MovieClip.prototype.play.call(this);
        }
        this.gotoAndStop = function(positionOrLabel){
            cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
        }
        this.stop = function(){
            cjs.MovieClip.prototype.stop.call(this);
        }
    }).prototype = p = new cjs.MovieClip();
    // symbols:
    
    
    
    (lib.centrisys_map_atlas_1 = function() {
        this.initialize(ss["centrisys_map_atlas_1"]);
        this.gotoAndStop(0);
    }).prototype = p = new cjs.Sprite();
    // helper functions:
    
    function mc_symbol_clone() {
        var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
        clone.gotoAndStop(this.currentFrame);
        clone.paused = this.paused;
        clone.framerate = this.framerate;
        return clone;
    }
    
    function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
        var prototype = cjs.extend(symbol, cjs.MovieClip);
        prototype.clone = mc_symbol_clone;
        prototype.nominalBounds = nominalBounds;
        prototype.frameBounds = frameBounds;
        return prototype;
        }
    
    
    (lib.Two = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        this._renderFirstFrame();
    
    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(0,0,0,0);
    
    
    (lib.Three = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("2", "normal 700 30px 'Roboto'", "#E07C00");
        this.text.lineHeight = 42;
        this.text.parent = this;
        this.text.setTransform(11.6,10.4);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("AjHjHIGPAAIAAGPImPAAg");
        this.shape.setTransform(20,23);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("AjHDIIAAmPIGPAAIAAGPg");
        this.shape_1.setTransform(20,23);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.Three, new cjs.Rectangle(-1,2,42,74.1), null);
    
    
    (lib.six_mc = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("4", "normal 700 30px 'Roboto'", "#E07C00");
        this.text.lineHeight = 42;
        this.text.parent = this;
        this.text.setTransform(9.95,7.85,0.9132,0.9132);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("Ai3C4IAAlvIFvAAIAAFuIltAA");
        this.shape.setTransform(18.35,18.35);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("Ai3C4IAAlvIFvAAIAAFuIltAAIAAABg");
        this.shape_1.setTransform(18.35,18.35);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.six_mc, new cjs.Rectangle(-1,-1,38.7,85.3), null);
    
    
    (lib.OneB = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("1", "normal 700 30px 'Roboto'", "#E07C00");
        this.text.lineHeight = 42;
        this.text.parent = this;
        this.text.setTransform(27.25,11.8);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("AjHjHIGPAAIAAGPImPAAg");
        this.shape.setTransform(35.6,23);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("AjHDIIAAmPIGPAAIAAGPg");
        this.shape_1.setTransform(35.6,23);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.OneB, new cjs.Rectangle(14.6,2,42,75.5), null);
    
    
    (lib.OneA = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("1", "normal 700 30px 'Roboto'", "#E07C00");
        this.text.lineHeight = 42;
        this.text.parent = this;
        this.text.setTransform(24.4,19.8);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("AjHjHIGPAAIAAGPImPAAg");
        this.shape.setTransform(32.6,31.4);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("AjHDIIAAmPIGPAAIAAGPg");
        this.shape_1.setTransform(32.6,31.4);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.OneA, new cjs.Rectangle(11.6,10.4,42,75.1), null);
    
    
    (lib.Nine = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("6", "normal 700 30px 'Roboto'", "#E07C00");
        this.text.lineHeight = 42;
        this.text.parent = this;
        this.text.setTransform(10.8,12);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("AjHjHIGPAAIAAGPImPAAg");
        this.shape.setTransform(20,23);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("AjHDIIAAmPIGPAAIAAGPg");
        this.shape_1.setTransform(20,23);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.Nine, new cjs.Rectangle(-1,2,42,75.7), null);
    
    
    (lib.Hotspot = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("3", "normal 700 30px 'Roboto'", "#E07C00");
        this.text.lineHeight = 42;
        this.text.parent = this;
        this.text.setTransform(11.2,28.8);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("AjHjHIGPAAIAAGPImPAAg");
        this.shape.setTransform(20,41.6);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("AjHDIIAAmPIGPAAIAAGPg");
        this.shape_1.setTransform(20,41.6);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.Hotspot, new cjs.Rectangle(-1,20.6,42,73.9), null);
    
    
    (lib.FiveB = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("3", "normal 700 30px 'Roboto'", "#E07C00");
        this.text.lineHeight = 42;
        this.text.parent = this;
        this.text.setTransform(24,27);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("AjHjHIGPAAIAAGPImPAAg");
        this.shape.setTransform(31.85,39.05);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("AjHDIIAAmPIGPAAIAAGPg");
        this.shape_1.setTransform(31.85,39.05);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.FiveB, new cjs.Rectangle(10.9,18.1,42,74.6), null);
    
    
    (lib.Eight = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        // Layer_1
        this.text = new cjs.Text("5", "normal 700 35px 'Roboto'", "#E07C00");
        this.text.lineHeight = 48;
        this.text.parent = this;
        this.text.setTransform(9.6,10);
        if(!lib.properties.webfonts['Roboto']) {
            lib.webFontTxtInst['Roboto'] = lib.webFontTxtInst['Roboto'] || [];
            lib.webFontTxtInst['Roboto'].push(this.text);
        }
    
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#E07C00").ss(2,1,1).p("AjHjHIGPAAIAAGPImPAAg");
        this.shape.setTransform(20,23);
    
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("rgba(255,255,255,0.439)").s().p("AjHDIIAAmPIGPAAIAAGPg");
        this.shape_1.setTransform(20,23);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = getMCSymbolPrototype(lib.Eight, new cjs.Rectangle(-1,2,42,73.7), null);
    
    
    // stage content:
    (lib.centrisys_map = function(mode,startPosition,loop,reversed) {
    if (loop == null) { loop = true; }
    if (reversed == null) { reversed = false; }
        var props = new Object();
        props.mode = mode;
        props.startPosition = startPosition;
        props.labels = {};
        props.loop = loop;
        props.reversed = reversed;
        cjs.MovieClip.apply(this,[props]);
    
        this.actionFrames = [0];
        this.isSingleFrame = false;
        // timeline functions:
        this.frame_0 = function() {
            if(this.isSingleFrame) {
                return;
            }
            if(this.totalFrames == 1) {
                this.isSingleFrame = true;
            }
            /* add event listeners */
            this.one_a_mc.addEventListener("click", function() {
                window.location = "#/thk-series-sludge-thickener";
            }, false);
            this.one_b_mc.addEventListener("click", function() {
                window.location = "#/thk-series-sludge-thickener";
            }, false);
            /*
            this.two_mc.addEventListener("click", function() {
                window.location = "#/home";
            }, false);
            */
            this.three_mc.addEventListener("click", function() {
                window.location = "#/pondus";
            }, false); //this is #2
            /*
            this.four_mc.addEventListener("click", function() {
                window.location = "#/digester-storage-tanks";
            }, false);
            */ 
            
            this.five_a_mc.addEventListener("click", function() {
                window.location = "#/cs";
            }, false); // this is #3
            this.five_b_mc.addEventListener("click", function() {
                window.location = "#/cs";
            }, false);
            
            this.six_mc.addEventListener("click", function() {
                window.location = "#/calprex";
            }, false);
            
            /*
            this.seven_mc.addEventListener("click", function() {
                window.location = "#/digester-storage-tanks";
            }, false);
            */
            this.eight_mc.addEventListener("click", function() {
                window.location = "#/magprex";
            }, false);
            this.nine_mc.addEventListener("click", function() {
                //showWindow("nine");
                window.location = "#/dlt-low-temp-belt-dryer";
            }, false);
            
            /* set the opacity for #2 as it currently doesn't link anywhere */
            this.two_mc.alpha = .3;
        }
    
        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));
    
        // Nine
        this.six_mc = new lib.six_mc();
        this.six_mc.name = "six_mc";
        this.six_mc.setTransform(964.85,502.15,1,1,0,0,0,18.4,33.4);
    
        this.eight_mc = new lib.Eight();
        this.eight_mc.name = "eight_mc";
        this.eight_mc.setTransform(461.55,273.4,0.9132,0.9132,0,0,0,29.2,31.2);
    
        this.five_b_mc = new lib.FiveB();
        this.five_b_mc.name = "five_b_mc";
        this.five_b_mc.setTransform(301.3,329.65,0.9132,0.9132,0,0,0,32.3,31.2);
    
        this.three_mc = new lib.Three();
        this.three_mc.name = "three_mc";
        this.three_mc.setTransform(1036.95,282.4,0.9132,0.9132,0,0,0,29.2,31.2);
    
        this.one_b_mc = new lib.OneB();
        this.one_b_mc.name = "one_b_mc";
        this.one_b_mc.setTransform(847.85,226.95,0.9132,0.9132,0,0,0,32.4,31.2);
    
        this.two_mc = new lib.Two();
        this.two_mc.name = "two_mc";
        this.two_mc.setTransform(843.35,92.4,0.9132,0.9132,0,0,0,29.2,31.2);
        this.two_mc.alpha = 0.4805;
    
        this.nine_mc = new lib.Nine();
        this.nine_mc.name = "nine_mc";
        this.nine_mc.setTransform(122.35,343.15,0.9132,0.9132,0,0,0,29.2,31.2);
    
        this.one_a_mc = new lib.OneA();
        this.one_a_mc.name = "one_a_mc";
        this.one_a_mc.setTransform(738.5,239.95,0.9132,0.9132,0,0,0,31.8,31.2);
    
        this.five_a_mc = new lib.Hotspot();
        this.five_a_mc.name = "five_a_mc";
        this.five_a_mc.setTransform(871.4,332.65,0.9132,0.9132,0,0,0,29.2,31.2);
    
        this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.five_a_mc},{t:this.one_a_mc},{t:this.nine_mc},{t:this.two_mc},{t:this.one_b_mc},{t:this.three_mc},{t:this.five_b_mc},{t:this.eight_mc},{t:this.six_mc}]}).wait(1));
    
        // map
        this.instance = new lib.centrisys_map_atlas_1();
        this.instance.setTransform(0,0,0.7924,0.7924);
    
        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
    
        this._renderFirstFrame();
    
    }).prototype = p = new lib.AnMovieClip();
    p.nominalBounds = new cjs.Rectangle(623.5,300,611.2,294.4);
    // library properties:
    lib.properties = {
        id: '3BBBCBE75488FE49BEF0E172D80A3BD4',
        width: 1247,
        height: 600,
        fps: 24,
        color: "#FFFFFF",
        opacity: 1.00,
        webfonts: {},
        manifest: [
            {src:"images/centrisys_map_atlas_1.png", id:"centrisys_map_atlas_1"}
        ],
        preloads: []
    };
    
    
    
    // bootstrap callback support:
    
    (lib.Stage = function(canvas) {
        createjs.Stage.call(this, canvas);
    }).prototype = p = new createjs.Stage();
    
    p.setAutoPlay = function(autoPlay) {
        this.tickEnabled = autoPlay;
    }
    p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
    p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
    p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
    p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }
    
    p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }
    
    an.bootcompsLoaded = an.bootcompsLoaded || [];
    if(!an.bootstrapListeners) {
        an.bootstrapListeners=[];
    }
    
    an.bootstrapCallback=function(fnCallback) {
        an.bootstrapListeners.push(fnCallback);
        if(an.bootcompsLoaded.length > 0) {
            for(var i=0; i<an.bootcompsLoaded.length; ++i) {
                fnCallback(an.bootcompsLoaded[i]);
            }
        }
    };
    
    an.compositions = an.compositions || {};
    an.compositions['3BBBCBE75488FE49BEF0E172D80A3BD4'] = {
        getStage: function() { return exportRoot.stage; },
        getLibrary: function() { return lib; },
        getSpriteSheet: function() { return ss; },
        getImages: function() { return img; }
    };
    
    an.compositionLoaded = function(id) {
        an.bootcompsLoaded.push(id);
        for(var j=0; j<an.bootstrapListeners.length; j++) {
            an.bootstrapListeners[j](id);
        }
    }
    
    an.getComposition = function(id) {
        return an.compositions[id];
    }
    
    
    an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
        var lastW, lastH, lastS=1;		
        window.addEventListener('resize', resizeCanvas);		
        resizeCanvas();		
        function resizeCanvas() {			
            var w = lib.properties.width, h = lib.properties.height;			
            var iw = window.innerWidth, ih=window.innerHeight;			
            var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
            if(isResp) {                
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
                    sRatio = lastS;                
                }				
                else if(!isScale) {					
                    if(iw<w || ih<h)						
                        sRatio = Math.min(xRatio, yRatio);				
                }				
                else if(scaleType==1) {					
                    sRatio = Math.min(xRatio, yRatio);				
                }				
                else if(scaleType==2) {					
                    sRatio = Math.max(xRatio, yRatio);				
                }			
            }
            domContainers[0].width = w * pRatio * sRatio;			
            domContainers[0].height = h * pRatio * sRatio;
            domContainers.forEach(function(container) {				
                container.style.width = w * sRatio + 'px';				
                container.style.height = h * sRatio + 'px';			
            });
            stage.scaleX = pRatio*sRatio;			
            stage.scaleY = pRatio*sRatio;
            lastW = iw; lastH = ih; lastS = sRatio;            
            stage.tickOnUpdate = false;            
            stage.update();            
            stage.tickOnUpdate = true;		
        }
    }
    an.handleSoundStreamOnTick = function(event) {
        if(!event.paused){
            var stageChild = stage.getChildAt(0);
            if(!stageChild.paused || stageChild.ignorePause){
                stageChild.syncStreamSounds();
            }
        }
    }
    an.handleFilterCache = function(event) {
        if(!event.paused){
            var target = event.target;
            if(target){
                if(target.filterCacheList){
                    for(var index = 0; index < target.filterCacheList.length ; index++){
                        var cacheInst = target.filterCacheList[index];
                        if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
                            cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
                        }
                    }
                }
            }
        }
    }
    
    
    })(createjs = createjs||{}, AdobeAn = AdobeAn||{});
    var createjs, AdobeAn;