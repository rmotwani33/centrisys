const STATUS = "color: yellow; background-color: blue; padding: 2px";
const SERVICE_WORKER = "color: yellow; background-color: purple; padding: 2px";
const WARNING = "color: black; background-color: yellow; padding: 2px";
const ERROR = "color: white; background-color: red; padding: 2px";
const DEBUG = "color: red; background-color: white; padding: 2px";
const SECTION = "color: gray; background-color: green; padding: 2px";
const DEBUG_VERSION = 3;

var version = "v0.1 beta";
var api_endpoint = "https://cms.centrisys-cnp.com"; //TODO: Change this when environment changes
var auth_endpoint = "/users/me";
var media_endpoint = "/items/media_item"
var authenticated = false;
var firstName = "";
var page_endpoint = "";
var processing = true;
var messageQueue = new Array();
var CACHE_PREFIX = "CENT_";

var myHeaders = new Headers();
myHeaders.append("Authorization", "Bearer This_is_my_very_secure_token_for_access");

var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
};

if ('caches' in window) {
    console.log("Caches are supported");
} else {
    console.log("Caches are not supported");
}

var appVersionNumber = 0;

var galleriesCache = CACHE_PREFIX + "Gallery";
var sectionsCache = CACHE_PREFIX + "Sections";
var pagesCache = CACHE_PREFIX + "Pages";
var staticFilesCache = CACHE_PREFIX + "StaticFiles";
var mediaCaches = CACHE_PREFIX + "Media";

var paused = true;
var currentView = localStorage["currentView"];

if (!currentView) {
    currentView = new Object();
    currentView.url = "#/";
    currentView.template = "templates/home.htm";
    localStorage.setItem("currentView", JSON.stringify(currentView));
}
var currentViewObject = localStorage.getItem("currentView");
currentView = JSON.parse(currentViewObject);

var lastView = localStorage["lastView"];
if (!lastView) {
    lastView = new Object();
    lastView.url = "#/";
    lastView.template = "templates/home.htm";
    localStorage.setItem("lastView", JSON.stringify(lastView));
}
var lastViewObject = localStorage.getItem("lastView");
lastView = JSON.parse(lastViewObject);


var messageProxy = new Proxy(messageQueue, {
    set: function (target, key, value) {
        manageMessageList(key, value);
        target[key] = value;
        return true;
    }
});

function manageMessageList(key, value) {
    if (key !== "length") {
        $("#statusBar").show();
        $("#status").text("");
        setTimeout(function () {
            $("#status").text(value);
            messageProxy.splice(key, 1);
            setTimeout(function () {
                $("#statusBar").hide();
            }, 1000);
        }, 500);
    }
}

var App = {
    init: function () {
        console.log("%c Init with Debug version: " + DEBUG_VERSION, DEBUG);
        $("#logout").hide();

        App.runOnce();
        App.addHandlers();
        $(".screen-loader").hide();

        if (App.isAuthenticated()) {
            $("#logout").show();

            if (navigator.serviceWorker.controller) {
  // At this point, the old content will have been purged and
  // the fresh content will have been added to the cache.
  // It's the perfect time to display a "New content is
  // available; please refresh." message in your web app.
  console.log('New content is available; please refresh.');

  const pushState = window.history.pushState;

  window.history.pushState = function () {
    // make sure that the user lands on the "next" page
    pushState.apply(window.history, arguments);

    // makes the new service worker active
    installingWorker.postMessage('SKIP_WAITING');
  };
} 

            var downloadGUID = sessionStorage.getItem("download");
            var download = "<a href=\"" + api_endpoint + "/assets/" + downloadGUID + "\">Download Presentation</a>";

            //$(".terms-conditions").append(download);
            $("#docDownload").html(download);
            
            
            $('.flexslider').flexslider({
                animation: "slide",
                prevText: "",
                nextText: ""
            });

            // Full Width Sliders
            $('.full-width-flexslider').flexslider({
                animation: "slide",
                prevText: "",
                nextText: "",
                controlNav: false
            });

            // Video Gallery Popups
            $('.popup-youtube').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });
            //check browser compatibiliy
            var browser = (function (agent) {
                switch (true) {
                    case agent.indexOf("edge") > -1: return "edge";
                    case agent.indexOf("edg") > -1: return "chrome-edge";
                    case agent.indexOf("opr") > -1 && !!window.opr: return "opera";
                    case agent.indexOf("chrome") > -1 && !!window.chrome: return "chrome";
                    case agent.indexOf("trident") > -1: return "ie";
                    case agent.indexOf("firefox") > -1: return "firefox";
                    case agent.indexOf("safari") > -1: return "safari";
                    default: return "other";
                }
            })(window.navigator.userAgent.toLowerCase());
            if (browser !== "chrome-edge") {
                App.checkCookie();
            }
        }
    },
    addHandlers: function () {
        $(".navbar-burger").promise().done(function (arg1) {
            $(".navbar-burger").click(function (e) {
                e.preventDefault();
                $(".navbar-burger").toggleClass("is-active");
                $(".navbar-menu").toggleClass("is-active");
            });
        });

        $("#logout").click(function (e) {
            e.preventDefault();
            //TODO: STATIC - NO LOGOUT
            /*
            sessionStorage.removeItem("bearerToken");
            sessionStorage.removeItem("authenticated");
            sessionStorage.removeItem("download");
            //redirect to login screen
            window.location = "#/login";
            $.router.run("#login", "login");
            location.reload();
            */
        });

        $("#dismiss-message").click(function (e) {
            e.preventDefault();
            $("#login-form .notification").addClass("is-hidden");
        });

        $("#login .button").click(function (e) {
            e.preventDefault();
            var username = $.trim($("#username").val());
            var bearer_token = $.trim($("#password").val());
            var rememberMe = $("#rememberMe").val();
            var login = false;
            var settings = {
                "url": api_endpoint + auth_endpoint,
                "method": "GET",
                "timeout": 0,
                "dataType": "json",
                "headers": {
                    "Authorization": "Bearer " + bearer_token
                },
                "statusCode": {
                    401: function () {
                        console.log("401: Unathorized");
                        login = false;
                        $("#login-form .notification").removeClass("is-hidden");
                    }
                }
            };

            $.ajax(settings).done(function (response) {
                var jsonResponse = response.data;
                firstName = jsonResponse.first_name;
                var lastName = jsonResponse.last_name;
                var presentationDownload = jsonResponse.presentation_download;
                login = true;
                authenticated = true;
                sessionStorage.setItem("authenticated", authenticated);
                if (rememberMe) {
                    sessionStorage.setItem("download", presentationDownload);
                    sessionStorage.setItem("email", username);
                    sessionStorage.setItem("bearerToken", bearer_token);
                }

                window.location = "#/home";
                $.router.run("#main-view", "home");
                location.reload();
            });
        });
    },
    isAuthenticated: function () {
        $(".screen-loader").hide();
        //TODO: FOR STATIC CONTENT
        /*
        var sessionAuthenticated = sessionStorage.getItem("authenticated");
        if (!sessionAuthenticated) {
            window.location = "#/login";
            return false;
        }
        */
        return true;
    },
    setVersion: function () {
        $("#version").text(version);
    },
    registerServiceWorker: function () { //Register the service worker
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.addEventListener('message', event => {
                console.log(event.data.msg, event.data.url);
                App.handleServiceWorkerMessage(event.data.msg, event.data.url);
            });
            navigator.serviceWorker.register('/service-worker.js').then(reg => {
                reg.installing; // the installing worker, or undefined
                reg.waiting; // the waiting worker, or undefined
                reg.active; // the active worker, or undefined

                reg.addEventListener('updatefound', () => {
                    const newWorker = reg.installing;
                    newWorker.state;
                    newWorker.addEventListener('statechange', () => {
                        console.log("Service Worker::: " + newWorker.state);
                    });
                });
            });
            navigator.serviceWorker.addEventListener('controllerchange', () => {
                console.log("Service Worker: controller change");
            });
        }
    },
    handleServiceWorkerMessage: function (message, url) {
        var message = "caching: " + App.getDomain(url) + " " + App.getFileName(url);
        //console.log("%cService Worker: " + message + ": " + App.getDomain(url) + " " + App.getFileName(url), SERVICE_WORKER);
        messageProxy.push(message);
    },
    isReachable: function (url) {
        return fetch(url, { method: 'HEAD', mode: 'no-cors' })
            .then(function (resp) {
                return resp && (resp.ok || resp.type === 'opaque');
            })
            .catch(function (err) {
                //console.warn('[conn test failure]:', err);
            });
    },
    getServerUrl: function () {
        return window.location.origin;
    },
    runOnce: function () {
        $("#status").text("updating content - downloading assets");
        App.registerServiceWorker();
        App.setVersion();
        localStorage["initialized"] = true;
        //set last view to home
        if (!currentView) {
            localStorage["currentView"] = defaultRoute;
        }
    },
    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    checkCookie: function () {
        var warning = App.getCookie("warning");
        if (warning != "") {
            //console.log("don't show message");
        } else {
            //TODO: show a prettier warning
            // alert("This app is best experienced using Microsoft Edge");
            App.setCookie("warning", true, 365);
        }
    },
    //ContentType = galleriesCache + version
    handlePageContent: function (contentType, contentId, page) {//Charlie!

        //const cacheName = 'pages';
        const cacheName = pagesCache;
        //console.log("%c With Cache type: " + cacheName, DEBUG);
        const url = page;
        //console.log("Proceeding! " + cacheName);
        //console.log("%c Proccessing page: " + page, DEBUG);
        //console.log("Page id is " + contentId + " !!!");
        //console.log("Opening cache " + cacheName);

        caches.open(cacheName).then(cache => {
            cache.match(url).then(response => {
                $.each(response, function (key, value) {
                    console.log("response is " + response);
                    if (key === "json") {
                        response.json().then(data => {
                            $.each(data, function (key, value) {
                                console.log(key + ':' + value);
                                var json = value;
                                console.log("Page JSON is " + JSON.stringify(json));
                                console.log("Page Title " + json.title);
                                console.log("Page Version: " + json.version);
                                $("#pageVersionId").text("v." + json.version);
                                console.log("My version is " + version);
                                //if version is set to 0 - don't do anything
                                if (json.version > 0) {
                                    //otherwise process the information as new
                                    //console.log("Page sections " + json.content);
                                    //console.log("Page sections " + JSON.stringify(json.content));
                                    var index = 0;
                                    $.each(json.content, function (subkey, subvalue) {
                                        index++;
                                        //console.log("%c Collection name: " + subvalue.collection, WARNING);//charlie
                                        //console.log("Collection Id " + subvalue.item);
                                        //console.log("JSON " + JSON.stringify(json));
                                        switch (subvalue.collection) {
                                            case "content_section":
                                                console.log("-=Processing content section=-")
                                                App.processContentSection(subvalue.item, index);
                                                break;
                                            case "media_gallery":
                                                console.log("%c Processing Media Gallery", STATUS);
                                                App.processMediaGallery(subvalue.item, index);
                                                break;
                                            default:
                                            //TODO: handle links
                                            //anchorLinks.push(sectionType);
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
            });
        });
    }, //section id:for content, index: for placement inside of the DOM
    processContentSection: function (sectionId, index) {
        console.log("%c Proccessing section: " + sectionId, DEBUG);
        //const cacheName = 'sections';
        const cacheName = sectionsCache;
        var section_endpoint = api_endpoint + "/items/content_section/" + sectionId + "?fields=*.*";
        const url = section_endpoint;
        /*
         * content_type:
         *      product overview
         *      performance data
         *      case study
         *      how it works
         *      installations
         */
        //console.log("my cache name " + cacheName);
        console.log("my url is " + url);
        caches.open(cacheName).then(cache => {
            cache.match(url).then(response => {
                $.each(response, function (key, value) {
                    if (key === "json") {
                        response.json().then(data => {
                            var contentIndex = 0;
                            $.each(data, function (key, value) {
                                contentIndex++;
                                console.log(key + ':' + value);
                                var json = value;
                                // json.content_type: content_section or media_gallery

                                console.log("Page Title " + json.title);
                                console.log("Page Subtitle: " + json.subtitle);
                                console.log("Page Subtitle: " + json.content);
                                console.log("Page Content_Type " + json.content_type);
                                //console.log("JSON + " + JSON.stringify(json));
                                switch (json.content_type) {
                                    case "product overview":
                                        console.log("%c Processing Product Overview", SECTION);
                                        App.processProductOverview(json, contentIndex);
                                        break;
                                    case "performance data":
                                        console.log("%c Processing Performace Data", SECTION);
                                        App.processProductOverview(json, contentIndex);
                                        break;
                                    case "case study":
                                        console.log("%c Processing Case study", SECTION);
                                        App.processCaseStudy(json, contentIndex);
                                        break;
                                    case "how it works":
                                        console.log("%c Processing How it works", SECTION);
                                        //console.log("content passed is " + JSON.stringify(json));
                                        App.processHowItWorks(json, contentIndex);
                                        break;
                                    case "installations":
                                        console.log("%c Processing Installations", SECTION);
                                        App.processInstallations(json, contentIndex);
                                        break;
                                    default:
                                        break;
                                }
                            });
                        });
                    }
                });
            });
        });
    },
    processProductOverview: function (json, index) {
        App.processSection("product-overview", json, index);

    },
    processPerformanceData: function (json, index) {
        App.processSection("performance-data", json, index);
    },
    processHowItWorks: function (json, index) {
        //console.log("content passed is " + JSON.stringify(json));
        App.processSection("how-it-works", json, index);
    },
    processCaseStudy: function (json, index) {
        App.processSection("case-study", json, index);
    },
    processInstallations: function (json, index) {
        App.processSection("installations", json, index);
    },
    processMediaGallery: function (json, index) {
        //console.log("++++++++++++-------- Processing Video Gallery");
    },
    processSection: function (prefix, json, index) {
        /* Left side */
        console.log("content passed is " + JSON.stringify(json));
        console.log("My prefix is " + prefix);
        console.log("My title is " + json.title)
        $("." + prefix + "-title").html(mmd(json.title));
        $("." + prefix + "-subtitle").html(mmd(json.subtitle));
        $("." + prefix + "-content").html(json.content);
        /* Right side (Gallery) */
        var galleryItems = json.media;
        if (galleryItems.length < 1) {
            //ignore it
        } else if (galleryItems.length == 1) {
            var galleryItem = galleryItems[0];
            var galleryId = galleryItem.media_gallery_id;
            console.log("Media id " + galleryId);

            /* */
            //const cacheName = 'galleries';
            const cacheName = galleriesCache;
            var gallery_endpoint = api_endpoint + "/items/media_item/" + galleryId + "?fields=*.*";
            const url = gallery_endpoint;

             caches.open(cacheName).then(cache => {
                cache.match(url).then(response => {
                    $.each(response, function (key, value) {
                        if (key === "json") {
                            response.json().then(data => {
                                var contentIndex = 0;
                                $.each(data, function (subkey, subvalue) {
                                    console.log("Gallery Name " + subvalue.title);
                                    console.log("Gallery  " + subvalue.media);
                                    console.log("Gallery items are: " + subvalue.media.length);

                                    //clear out the current gallery
                                    var flex = $("#" + prefix + "-gallery-flexslider");
                                    while (flex.data("flexslider") != null && flex.data("flexslider").count > 0) {
                                        flex.data("flexslider").removeSlide(0);
                                    }
                                    //add the new media
                                    $.each(subvalue.media, function (subsubkey, subsubvalue) {
                                        var mediaId = subsubvalue.directus_files_id;
                                        var slide = "<li><img src=\"" + api_endpoint + "/assets/" + mediaId + "\"/></li>";
                                        if (flex.data("flexslider") != null) {
                                            flex.data("flexslider").addSlide(slide);
                                        }
                                    });

                                });
                            });
                        }
                    });
                });
            });
        }
    },
    notEmpty: function (value) {
        if (value != null && value != "undefined" && value != "") {
            return true;
        }
        return false;
    },
    getFileName: function (url) {
        var filename = url.split('/').pop();
        filename.substr(0, filename.lastIndexOf('.'));
        return filename.substr(0, 20);
    },
    getDomain: function (url) {
        var domain = (new URL(url));
        domain = domain.hostname;
        return domain.substr(0, 20);
    }
};

// Visit website click event
$("#visit-website").click(function (e) {
    e.preventDefault();
    window.open('http://www.centrisys-cnp.com/')
});

$("#pdf").click(function (e) {
    e.preventDefault();
    window.open('http://www.centrisys-cnp.com/')
});
//#region Routes
var routes = {},
    defaultRoute = 'home';

/*
routes["login"] = {
    url: "#/login",
    templateUrl: "templates/login.htm"
};
*/

routes["login"] = {
    url: "#/login",
    templateUrl: "templates/home.htm"
};


routes["pondus"] = {
    url: "#/pondus",
    templateUrl: "templates/pondus.htm"
};

routes["installation-kenosha"] = {
    url: "#/installation-kenosha",
    templateUrl: "templates/installation-kenosha.htm"
};

routes["installation-lohne-germany"] = {
    url: "#/installation-lohne-germany",
    templateUrl: "templates/installation-lohne-germany.htm"
};

routes["installation-nordhorn-germany"] = {
    url: "#/installation-nordhorn-germany",
    templateUrl: "templates/installation-nordhorn-germany.htm"
};

routes["installation-ratekau-germany"] = {
    url: "#/installation-ratekau-germany",
    templateUrl: "templates/installation-ratekau-germany.htm"
};

routes["installation-uelzen-germany"] = {
    url: "#/installation-uelzen-germany",
    templateUrl: "templates/installation-uelzen-germany.htm"
};

routes["installation-wolfsburg-germany"] = {
    url: "#/installation-wolfsburg-germany",
    templateUrl: "templates/installation-wolfsburg-germany.htm"
};

routes["installation-gifhorn-germany"] = {
    url: "#/installation-gifhorn-germany",
    templateUrl: "templates/installation-gifhorn-germany.htm"
};

routes["installation-naples-italy"] = {
    url: "#/installation-naples-italy",
    templateUrl: "templates/installation-naples-italy.htm"
};

routes["installation-goeppingen-germany"] = {
    url: "#/installation-goeppingen-germany",
    templateUrl: "templates/installation-goeppingen-germany.htm"
};

routes['calprex'] = {
    url: "#/calprex",
    templateUrl: "templates/calprex.htm"
};

routes["pilot-test-madison"] = {
    url: "#/pilot-test-madison",
    templateUrl: "templates/pilot-test-madison.htm"
};

routes["pilot-test-woodridge"] = {
    url: "#/pilot-test-woodridge",
    templateUrl: "templates/pilot-test-woodridge.htm"
};

routes['magprex-calprex'] = {
    url: "#/magprex-calprex",
    templateUrl: "templates/magprex-calprex.htm"
};

routes["dlt-low-temp-belt-dryer"] = {
    url: "#/dlt-low-temp-belt-dryer",
    templateUrl: "templates/dlt-low-temp-belt-dryer.htm"
};

routes["kenosha-resource-project"] = {
    url: "#/kenosha-resource-project",
    templateUrl: "templates/kenosha-resource-project.htm"
};

routes["kenosha-water-utility"] = {
    url: "#/kenosha-water-utility",
    templateUrl: "templates/kenosha-water-utility.htm"
};

routes["thk-primary"] = {
    url: "#/thk-primary",
    templateUrl: "templates/thk-primary.htm",
};

routes["thk-was"] = {
    url: "#/thk-was",
    templateUrl: "templates/thk-was.htm"
};

routes["pondus-2"] = {
    url: "#/pondus-2",
    templateUrl: "templates/pondus-2.htm"
};

routes['digesters'] = {
    url: "#/digesters",
    templateUrl: "templates/digesters.htm"
};

routes['cs'] = {
    url: "#/cs",
    templateUrl: "templates/cs.htm"
};

routes['kwu'] = {
    url: "#/kwu",
    templateUrl: "templates/kwu.htm"
};

routes["low-temp-belt-dryer"] = {
    url: "#/low-temp-belt-dryer",
    templateUrl: "templates/low-temp-belt-dryer.htm"
};

routes['biogas'] = {
    url: "#/biogas",
    templateUrl: "templates/biogas.htm"
};

routes['chp-units'] = {
    url: "#/chp-units",
    templateUrl: "templates/chp-units.htm"
};

routes["thermal-heat"] = {
    url: "#/thermal-heat",
    templateUrl: "templates/thermal-heat.htm"
};

routes['electrical-energy'] = {
    url: "#/electrical-energy",
    templateUrl: "templates/electrical-energy.htm"
};

routes['biosolids'] = {
    url: "#/biosolids",
    templateUrl: "templates/biosolids.htm"
};

routes["thk-series-sludge-thickener"] = {
    url: "#/thk-series-sludge-thickener",
    templateUrl: "templates/thk-series-sludge-thickener.htm"
};

routes["installation"] = {
    url: "#/installation",
    templateUrl: "templates/installation.htm"
};

routes["installation-kenosha-2"] = {
    url: "#/installation-kenosha-2",
    templateUrl: "templates/installation-kenosha-2.htm"
};

routes["installation-whitewater"] = {
    url: "#/installation-whitewater",
    templateUrl: "templates/installation-whitewater.htm"
};

routes["Installation - City of Wyoming Clean Water Plant"] = {
    url: "#/installation-wyoming",
    templateUrl: "templates/installation-wyoming.htm"
};

routes["installation-gainesville"] = {
    url: "#/installation-gainesville",
    templateUrl: "templates/installation-gainesville.htm"
};

routes["installation-meridian"] = {
    url: "#/installation-meridian",
    templateUrl: "templates/installation-meridian.htm"
};


routes["installation-san-antonio"] = {
    url: "#/installation-san-antonio",
    templateUrl: "templates/installation-san-antonio.htm"
};

routes["sharjah-municipality"] = {
    url: "#/sharjah-municipality",
    templateUrl: "templates/sharjah-municipality.htm"
};

routes['digester-storage-tanks'] = {
    url: "#/digester-storage-tanks",
    templateUrl: "templates/digester-storage-tanks.htm"
};

routes['lipp-storage-tank'] = {
    url: "#/lipp-storage-tank",
    templateUrl: "templates/lipp-storage-tank.htm"
};

routes['lipp-anaerobic-digester'] = {
    url: "#/lipp-anaerobic-digester",
    templateUrl: "templates/lipp-anaerobic-digester.htm"
};

routes['install-medina'] = {
    url: "#/install-medina",
    templateUrl: "templates/install-medina.htm"
};

routes['install-grandrapids'] = {
    url: "#/install-grandrapids",
    templateUrl: "templates/install-grandrapids.htm"
};

routes['install-fenton'] = {
    url: "#/install-fenton",
    templateUrl: "templates/install-fenton.htm"
};

routes['magprex'] = {
    url: "#/magprex",
    templateUrl: "templates/magprex.htm"
};

routes["installation-savage"] = {
    url: "#/installation-savage",
    templateUrl: "templates/installation-savage.htm"
};

routes["installation-valley-city"] = {
    url: "#/installation-valley-city",
    templateUrl: "templates/installation-valley-city.htm"
};

routes["installation-elgin"] = {
    url: "#/installation-elgin",
    templateUrl: "templates/installation-elgin.htm"
};

routes["installation-fort-collins"] = {
    url: "#/installation-fort-collins",
    templateUrl: "templates/installation-fort-collins.htm"
};

routes["installation-denver"] = {
    url: "#/installation-denver",
    templateUrl: "templates/installation-denver.htm"
};

routes["installation-salt-lake-city"] = {
    url: "#/installation-salt-lake-city",
    templateUrl: "templates/installation-salt-lake-city.htm"
};

routes['magprex-calprex-2'] = {
    url: "#/magprex-calprex-2",
    templateUrl: "templates/magprex-calprex-2.htm"
};

routes['service-repair'] = {
    url: "#/service-repair",
    templateUrl: "templates/service-repair.htm"
};

routes['centrifuge-repair'] = {
    url: "#/centrifuge-repair",
    templateUrl: "templates/centrifuge-repair.htm"
};

routes['centrifuge-spare-replacement-parts'] = {
    url: "#/centrifuge-spare-replacement-parts",
    templateUrl: "templates/centrifuge-spare-replacement-parts.htm"
};

routes['centrifuge-upgrades'] = {
    url: "#/centrifuge-upgrades",
    templateUrl: "templates/centrifuge-upgrades.htm"
};

routes['centrisys-service-agreements'] = {
    url: "#/centrisys-service-agreements",
    templateUrl: "templates/centrisys-service-agreements.htm"
};

routes['hc-model'] = {
    url: "#/hc-model",
    templateUrl: "templates/hc-model.htm"
};

routes['t-model'] = {
    url: "#/t-model",
    templateUrl: "templates/t-model.htm"
};

routes['dt-model'] = {
    url: "#/dt-model",
    templateUrl: "templates/dt-model.htm"
};

routes['custom-solution'] = {
    url: "#/custom-solution",
    templateUrl: "templates/custom-solution.htm"
};

routes['components'] = {
    url: "#/components",
    templateUrl: "templates/components.htm"
};

routes["history"] = {
    url: "#/history",
    templateUrl: "templates/history.htm"
};

routes["home"] = {
    url: "#/",
    templateUrl: "templates/home.htm"
};

routes["terms-and-conditions"] = {
    url: "#/terms-and-conditions",
    templateUrl: "templates/terms-and-conditions.htm"
};

routes["installation-grossklaeranlage"] = {
    url: "#/installation-grossklaeranlage",
    templateUrl: "templates/installation-grossklaeranlage.htm"
};

routes["installation-klaerwerk-mettingen"] = {
    url: "#/installation-klaerwerk-mettingen",
    templateUrl: "templates/installation-klaerwerk-mettingen.htm"
};

routes["installation-klaerwerk-erlangen"] = {
    url: "#/installation-klaerwerk-erlangen",
    templateUrl: "templates/installation-klaerwerk-erlangen.htm"
};

routes["installation-wasserzweckverband"] = {
    url: "#/installation-wasserzweckverband",
    templateUrl: "templates/installation-wasserzweckverband.htm"
};

// ADDDED TO GET TO THE NAMPA PAGE

routes["installation-nampa"] = {
    url: "#/installation-nampa",
    templateUrl: "templates/installation-nampa.htm"
};

routes["installation-salt-lake-city"] = {
    url: "#/installation-salt-lake-city",
    templateUrl: "templates/installation-salt-lake-city.htm"
};

routes["Installation-City of Wyoming Clean Water Plant"] = {
    url: "#/installation-wyoming",
    templateUrl: "templates/installation-wyoming.htm"
};

//for testing video online and off
routes["video"] = {
    url: "#/video",
    templateUrl: "templates/video.htm"
}; //routes

$.when($.ready)
    .then(function () {
        $.router.setData(routes);
    });

//.setDefault(defaultRoute);
//#endregion Routes

//#region Route Logic

$.when($.ready)
    .then(function () {
        $.router.run("#main-view", "home");
    });

$.router.onViewChange(function (e, viewRoute, route, params) {
    App.init();
    var route1 = route;
    console.log("🚀 ~ file: app.js ~ line 956 ~ route1", route1)
    $('html,body').scrollTop(0);
    //get the current last view
    var lastViewObject = localStorage.getItem("lastView");
    lastView = JSON.parse(lastViewObject);

    //set the last view to the last known page
    
    if (lastView.url === route.url) {
        console.log("skipping view assignment");
    } else {
        localStorage.setItem("lastView", JSON.stringify(route));
    }
    localStorage.setItem("currentView", JSON.stringify(route));
    // if()
    if(route.name.indexOf('installation') == -1){
        localStorage.setItem("detailView", JSON.stringify(route));
    }
   console.log("models",route1.name.includes("model"))
    window.location.href = window.location.href;
    document.cookie = "location=" + window.location.hash;


    $(".back-button").click(function (e, route) {
        e.preventDefault();
        var lastViewObject = localStorage.getItem("lastView");
        lastView = JSON.parse(lastViewObject);
        var detailViewObject = localStorage.getItem("detailView");
        detailView = JSON.parse(detailViewObject);
        console.log("🚀 ~ file: app.js ~ line 986 ~ detailView",detailView.url)
        var currentViewObject = localStorage.getItem("currentView");
        currentView = JSON.parse(currentViewObject);
        console.log("🚀 ~ file: app.js ~ line 989 ~ currentView", currentView)
        
        if(route1.name.includes("model") || route1.name.includes("custom")){
            window.location = "#/cs"
        }else if(route1.name.includes("kenosha-2") || route1.name.includes("whitewater") || route1.name.includes("City of Wyoming Clean Water Plant") || route1.name.includes("gainesville") || route1.name.includes("san-antonio") || route1.name == "sharjah-municipality"){
           window.location = "#/thk-series-sludge-thickener"
        }else if(route1.name.includes("installation-kenosha") || route1.name.includes("lohne-germany") || route1.name.includes("nordhorn-germany") || route1.name.includes("ratekau-germany") || route1.name.includes("uelzen-germany") || route1.name.includes("wolfsburg-germany") || route1.name.includes("gifhorn-germany") || route1.name.includes("naples-italy") || route1.name.includes("goeppingen-germany") || route1.name.includes("grossklaeranlage") || route1.name.includes("klaerwerk-mettingen") || route1.name.includes("klaerwerk-erlangen") || route1.name.includes("wasserzweckverband")){
            window.location = "#/pondus"
        }else if(route1.name == "pilot-test-madison" || route1.name == "pilot-test-woodridge"){
            window.location = "#/calprex"
        }else if(route1.name.includes("savage") || route1.name.includes("valley-city") || route1.name.includes("elgin") || route1.name.includes("fort-collins") || route1.name.includes("denver") || route1.name.includes("salt-lake-city") || route1.name.includes("meridian") || route1.name.includes("nampa")){
            window.location = "#/magprex"
        }else if(route1.name == "centrifuge-repair" || route1.name == "centrifuge-spare-replacement-parts" || route1.name == "centrifuge-upgrades"){
            window.location = "#/service-repair"
        }else if(route1.name == "kenosha-water-utility" || route1.name == "thk-primary" || route1.name == "thk-was" || route1.name == "pondus-2" || route1.name == "digesters" || route1.name == "kwu" || route1.name == "low-temp-belt-dryer" || route1.name == "biogas" || route1.name == "chp-units" || route1.name == "thermal-heat" || route1.name == "electrical-energy" || route1.name == "biosolids" ){
            window.location = "#/kenosha-resource-project"
        }else if(route1.name == detailView.name){
            window.location = "#/"
        }else if(currentView.previousUrl === '#/kenosha-resource-project'){
            window.location = currentView.previousUrl;
        }else {
            window.location = "#/"
        }
       
        return false;
    });
});
//#endregion Route Logic

//#region Util
// Footer Date
var currentYear = new Date().getFullYear();
document.getElementById("currentYear").innerHTML = currentYear;

// Fixed Navigation
$(function () {
    $(document).scroll(function () {
        var $nav = $(".is-fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });
});

// Image Map Tooltips
$('.tooltip').tooltipster({
    theme: ['tooltipster-noir', 'tooltipster-noir-customized'],
    contentAsHTML: true,
    arrow: false,
    trigger: 'custom', // add custom trigger
    triggerOpen: { // open tooltip when element is clicked, tapped (mobile) or hovered
        click: true,
        tap: true,
        mouseenter: true
    },
    triggerClose: { // close tooltip when element is clicked again, tapped or when the mouse leaves it
        click: true,
        scroll: false, // ensuring that scrolling mobile is not tapping!
        tap: true,
        mouseleave: true
    }
});

/* Detect landscape mode on page load */
window.onload = function () {
    checkOrientation();
};

/* Detect landscape mode on orientaton change */
window.addEventListener('orientationchange', function () {
    // console.log('ori change');
    checkOrientation();
});

function checkOrientation() {
    if (window.matchMedia("(orientation: portrait)").matches) {
        //alert("Please use landscape mode.");
        $.magnificPopup.open({
            items: {
                src: $('#landscapeModal').html(), // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            }
        });
        $(".popup-modal-dismiss").click(function () {
            $.magnificPopup.close();
        });
    }
    if (window.matchMedia("(orientation: landscape)").matches) {
        $.magnificPopup.close();
    }
}

function mmd(src) {
    var h = '';

    function escape(t) {
        return new Option(t).innerHTML;
    }
    function inlineEscape(s) {
        return escape(s)
            .replace(/!\[([^\]]*)]\(([^(]+)\)/g, '<img alt="$1" src="$2">')
            .replace(/\[([^\]]+)]\(([^(]+?)\)/g, '$1'.link('$2'))
            .replace(/`([^`]+)`/g, '<code>$1</code>')
            .replace(/(\*\*|__)(?=\S)([^\r]*?\S[*_]*)\1/g, '<strong>$2</strong>')
            .replace(/(\*|_)(?=\S)([^\r]*?\S)\1/g, '<em>$2</em>');
    }

    if (src != null) {
        src.replace(/^\s+|\r|\s+$/g, '').replace(/\t/g, '    ').split(/\n\n+/).forEach(function (b, f, R) {
            f = b[0];
            R =
                {
                    '*': [/\n\* /, '<ul><li>', '</li></ul>'],
                    '1': [/\n[1-9]\d*\.? /, '<ol><li>', '</li></ol>'],
                    ' ': [/\n    /, '<pre><code>', '</code></pre>', '\n'],
                    '>': [/\n> /, '<blockquote>', '</blockquote>', '\n']
                }[f];
            h +=
                R ? R[1] + ('\n' + b)
                    .split(R[0])
                    .slice(1)
                    .map(R[3] ? escape : inlineEscape)
                    .join(R[3] || '</li>\n<li>') + R[2] :
                    f == '#' ? '<h' + (f = b.indexOf(' ')) + '>' + inlineEscape(b.slice(f + 1)) + '</h' + f + '>' :
                        f == '<' ? b :
                            '<p>' + inlineEscape(b) + '</p>';
        });
        return h;
    }
}


//#endregion Util